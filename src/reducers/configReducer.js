import initialState from './initialState';

export const configReducer = (state = initialState.config, action) => {
    switch(action.type) {
        case 'CONFIG_UPDATE':
            return {
                ...action.data,
            };

        default:
            return state;
    }
};