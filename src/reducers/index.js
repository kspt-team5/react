import { combineReducers } from 'redux';
import { configReducer } from "./configReducer";
import { routerReducer } from 'react-router-redux';
import responseReducer from "./responseReducer";

const rootReducer = combineReducers({
    routing: routerReducer,
    config: configReducer,
    response: responseReducer,
});

export default rootReducer;
