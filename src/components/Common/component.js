import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import './common.css';

export default class Common extends Component {

    componentWillMount() {
    }

    render() {
        return (
            <div className='app-container'>
                <nav className="navbar sticky-top navbar-expand-sm navbar-dark bg-dark">
                    <div className="container">
                        <a className="navbar-brand text-light">
                            <img src={require('../../img/mailcat.jpg')} id="logo" className="d-inline-block align-top" alt="" />
                            MailCat
                        </a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar7">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="navbar-collapse collapse justify-content-stretch" id="navbar7">
                            <ul className="navbar-nav nav-fill">
                                <li className="nav-item">
                                    <Link to='/'><a className="nav-link" rel="nofollow" id="scrollup">Home</a></Link>
                                </li>
                                <li className="nav-item">
                                    <Link to='#settings'><a className="nav-link" id="settings">Settings</a></Link>
                                </li>
                                <li className="nav-item">
                                    <Link to='/faq'><a className="nav-link">FAQ</a></Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>


                <div className="modal fade" aria-hidden="true" id="modal-settings">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4 className="modal-title">Settings</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group row">
                                    <div className="col-12">
                                        <label for="inputEmail" className="sr-only">Email address</label>
                                        <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required autofocus />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-12">
                                        <label for="inputPassword" className="sr-only">Password</label>
                                        <input type="password" id="inputPassword" className="form-control" placeholder="Password" required />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-12">
                                        <label for="inputHost" className="sr-only">Host</label>
                                        <input type="url" id="inputHost" className="form-control" placeholder="Host" required />
                                    </div>
                                </div>

                                <div className="form-group row">
                                    <div className="col-12">
                                        <label for="inputPort" className="sr-only">Port</label>
                                        <input id="inputPort" className="form-control" placeholder="Port" required />
                                    </div>
                                </div>

                                <div className="form-group row slider-group">
                                    <div className="col-sm-6">Learning Threshold: <span id="learning-slider-value">30</span></div>
                                    <div className="col-sm-6 col-xs-12">
                                        <input id="count-slider" data-slider-min="10" data-slider-max="50" data-slider-step="1" data-slider-value="30" data-slider-id="learning-threshold-slider" type="text" value="30"/>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" className="btn btn-primary">OK</button>
                            </div>

                        </div>

                    </div>

                </div>

                {this.props.children}

                <footer>
                    <div>
                        <b>Contact us: team5kspt@gmail.com</b> <br />
                        The "MailCat" application is provided as is without any guarantees or warranty.
                    </div>
                </footer>


            </div>
        )
    }

}
