import React, { Component } from 'react';
import './Home.css';

export class HomePage extends Component {

    render() {
        return (
            <div>
                <div className="content">
                    <div className="container" id="inbox-head">
                        <div className="row">
                            <p1>You have <span id="email-list-size">0</span> new Emails</p1>
                        </div>
                        <div className="row">
                            <p1>To see more, check the list below</p1>
                        </div>
                    </div>
                    <div className="container" id="inbox-list">

                    </div>

                    <div className="container" id="test">
                        <button className="btn btn-dark" id="test-btn">Add New</button>
                    </div>

                </div>
            </div>
            )

    }

}