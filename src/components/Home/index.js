import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HomePage } from "./component";

class Home extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { config } = this.props;
        return (
            <HomePage config={config} />
            )

    }
}

const mapStateToProps = (state) => {
    const { config, response } = state;
    return {
        config,
        response,
    }
};
export default connect(
    mapStateToProps,
) (Home);
