import * as types from './_types';

function updateConfig(updated) {
    return {
        type: types.CONFIG_UPDATE,
        data: updated,
    };
}